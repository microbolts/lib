'use strict';

// Wrap into an anonymous function to hide public facing functions
(function () {
    const response_listeners = {};

    function sendMessage(type, payload, callback) {
        payload._mb_msgid = Math.floor(Math.random() * 10000);
        const data = {
            type: type,
            payload: payload,
            application: "MICROBOLTS"
        };

        const timeout = setTimeout(function () {
            response_listeners[payload._mb_msgid]({
                payload: {
                    error: 'TIMEOUT'
                }
            });

            delete response_listeners[payload._mb_msgid];
        }, 2000);

        response_listeners[payload._mb_msgid] = function (data) {
            clearTimeout(timeout);
            callback(data);

            delete response_listeners[payload._mb_msgid];
        };

        window.postMessage(data, "*");
    }

    window.addEventListener('message', function (msg) {
        const data = msg.data;

        if (data.application === 'MICROBOLTS_REPLY' &&
            data.payload._mb_msgid &&
            response_listeners[data.payload._mb_msgid] &&
            typeof response_listeners[data.payload._mb_msgid] === 'function'
        ) {
            response_listeners[data.payload._mb_msgid](data);
        }
    }, false);

    /**
     * Callback for the "IsAuthorized" callback
     *
     * @callback isAuthorizedCallback
     * @param {null|string} error - (optional) Error string.
     * @param {boolean} authorized - Whether the host is authorized or not.
     * @param {object} options - The options object.
     */

    /**
     * Check if the host is authorized to ask for payments
     *
     * @param {isAuthorizedCallback} callback - A callback to run.
     */

    function IsAuthorized(callback) {
        sendMessage('IS_AUTHORIZED', {}, function (response) {
            if (response.payload.error !== null) {
                return callback(response.payload.error, response.payload, null);
            }

            if (response.type !== 'IS_AUTHORIZED_RESPONSE') {
                return callback('TYPE_MISMATCH', null, null);
            }

            callback(null, response.payload.authorized, response.payload.options);
        });
    }

    /**
     * Callback for the "Authorize" callback
     *
     * @callback AuthorizeCallback
     * @param {null|string} error - (optional) Error string.
     * @param {object} options - The options object.
     */

    /**
     * Try to authorize the current host
     *
     * @param {AuthorizeCallback} callback - A callback to run.
     */

    function Authorize(callback) {
        sendMessage('AUTHORIZE', {}, function (response) {
            if (response.payload.error !== null) {
                return callback(response.payload.error, response.payload);
            }

            if (response.type !== 'AUTHORIZE_RESPONSE') {
                return callback('TYPE_MISMATCH', null);
            }

            callback(null, response.payload.options);
        });
    }

    /**
     * Callback for the "Pay" callback
     *
     * @callback PayCallback
     * @param {null|string} error - (optional) Error string.
     */

    /**
     * Try to authorize the current host
     *
     * @param {string} invoice - The invoice to pay
     * @param {PayCallback} callback - A callback to run.
     */

    function Pay(invoice, callback) {
        sendMessage('PAY', {invoice: invoice}, function (response) {
            if (response.payload.error !== null) {
                return callback(response.payload.error);
            }

            if (response.type !== 'PAY_RESPONSE') {
                return callback('TYPE_MISMATCH');
            }

            callback(null);
        });
    }

    window.microbolts = {
        IsAuthorized: IsAuthorized,
        Authorize: Authorize,
        Pay: Pay
    };
})();